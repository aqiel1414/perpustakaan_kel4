-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2020 at 03:17 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pelatihan2`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id` int(11) NOT NULL,
  `tgl_absensi` date DEFAULT NULL,
  `catatan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id`, `tgl_absensi`, `catatan`) VALUES
(1, '2019-10-23', 'Training di mulai'),
(2, '2019-10-27', 'Kondisi Cuaca kurang baik tetapi peserta cukup bertambah'),
(3, '2019-10-30', 'Sedang asik mengajar baterai laptop instruktur ketinggalan'),
(4, '2019-11-03', 'Cuaca Hujan Peserta Semangat'),
(5, '2019-11-10', 'INsya Allah keep istiqomah mulai install Linux'),
(6, '2019-11-13', 'Allhamdullilah seru dan tetap berkah'),
(7, '2019-11-20', 'Tetap Istiqomah , Pak Wahyudi biar telat datang tapi tetap datang Semangat'),
(8, '2019-12-01', 'Awalnya cuaca cerah , lalu hujan deras menemani kita semua ngoding');

-- --------------------------------------------------------

--
-- Table structure for table `absensi_mahasiswa`
--

CREATE TABLE `absensi_mahasiswa` (
  `id` int(11) NOT NULL,
  `id_absensi` int(11) DEFAULT NULL,
  `id_siswa` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `absensi_mahasiswa`
--

INSERT INTO `absensi_mahasiswa` (`id`, `id_absensi`, `id_siswa`) VALUES
(2, 1, 4),
(3, 1, 5),
(4, 1, 3),
(5, 1, 2),
(6, 2, 9),
(7, 2, 7),
(8, 2, 11),
(9, 2, 10),
(10, 2, 5),
(11, 2, 8),
(12, 2, 2),
(13, 2, 6),
(14, 3, 11),
(15, 3, 5),
(16, 3, 12),
(17, 3, 2),
(18, 3, 6),
(19, 4, 9),
(20, 4, 7),
(21, 4, 5),
(22, 4, 2),
(23, 4, 13),
(24, 5, 9),
(25, 5, 2),
(26, 5, 13),
(27, 6, 9),
(28, 6, 12),
(29, 6, 2),
(30, 6, 6),
(31, 6, 13),
(32, 7, 9),
(33, 7, 2),
(34, 7, 6),
(35, 7, 13),
(36, 8, 9),
(37, 8, 7),
(38, 8, 2),
(39, 8, 6);

-- --------------------------------------------------------

--
-- Table structure for table `admin_groups`
--

CREATE TABLE `admin_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_groups`
--

INSERT INTO `admin_groups` (`id`, `name`, `description`) VALUES
(1, 'webmaster', 'Webmaster'),
(11, 'admin', 'Administrator'),
(12, 'assesor', 'Asesor'),
(13, 'admin_tuk', 'ADMIN TUK'),
(14, 'peserta', 'Peserta'),
(15, 'pantek', 'Pantek');

-- --------------------------------------------------------

--
-- Table structure for table `admin_login_attempts`
--

CREATE TABLE `admin_login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`) VALUES
(1, '127.0.0.1', 'webmaster', '$2y$08$/X5gzWjesYi78GqeAv5tA.dVGBVP7C1e1PzqnYCVe5s1qhlDIPPES', NULL, NULL, NULL, NULL, NULL, NULL, 1451900190, 1605372146, 1, 'Webmaster', ''),
(2, '127.0.0.1', 'admin', '$2y$08$7Bkco6JXtC3Hu6g9ngLZDuHsFLvT7cyAxiz1FzxlX5vwccvRT7nKW', NULL, NULL, NULL, NULL, NULL, NULL, 1451900228, 1465489580, 1, 'Admin', ''),
(3, '127.0.0.1', 'manager', '$2y$08$snzIJdFXvg/rSHe0SndIAuvZyjktkjUxBXkrrGdkPy1K6r5r/dMLa', NULL, NULL, NULL, NULL, NULL, NULL, 1451900430, 1531790898, 1, 'Manager', ''),
(4, '127.0.0.1', 'staff', '$2y$08$NigAXjN23CRKllqe3KmjYuWXD5iSRPY812SijlhGeKfkrMKde9da6', NULL, NULL, NULL, NULL, NULL, NULL, 1451900439, 1465489590, 1, 'Staff', NULL),
(5, '::1', 'observer', '$2y$08$16kdP9SbQFi.HMHgPB.PPOGFfjMa471MeOLFsEr9cRz21FIqtwgXO', NULL, NULL, NULL, NULL, NULL, NULL, 1531506497, NULL, 1, 'OBSERVER', 'ALMARJAN');

-- --------------------------------------------------------

--
-- Table structure for table `admin_users_groups`
--

CREATE TABLE `admin_users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_users_groups`
--

INSERT INTO `admin_users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 4, 5),
(6, 5, 4);

-- --------------------------------------------------------

--
-- Table structure for table `api_access`
--

CREATE TABLE `api_access` (
  `id` int(11) UNSIGNED NOT NULL,
  `key` varchar(40) NOT NULL DEFAULT '',
  `controller` varchar(50) NOT NULL DEFAULT '',
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `api_keys`
--

CREATE TABLE `api_keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT 0,
  `is_private_key` tinyint(1) NOT NULL DEFAULT 0,
  `ip_addresses` text DEFAULT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `api_keys`
--

INSERT INTO `api_keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 0, 'anonymous', 1, 1, 0, NULL, 1463388382);

-- --------------------------------------------------------

--
-- Table structure for table `api_limits`
--

CREATE TABLE `api_limits` (
  `id` int(11) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `count` int(10) NOT NULL,
  `hour_started` int(11) NOT NULL,
  `api_key` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `api_logs`
--

CREATE TABLE `api_logs` (
  `id` int(11) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `method` varchar(6) NOT NULL,
  `params` text DEFAULT NULL,
  `api_key` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` varchar(1) NOT NULL,
  `response_code` smallint(3) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `headline_id` tinytext DEFAULT NULL,
  `headline_jp` tinytext CHARACTER SET utf8 DEFAULT NULL,
  `content_id` text DEFAULT NULL,
  `content_jp` text CHARACTER SET utf8 DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `headline_id`, `headline_jp`, `content_id`, `content_jp`, `updatetime`, `user_id`) VALUES
(1, 'Banjir Meme Lucu Jerman Dilibas Korsel', ' テストコンテンツ', '<p><a href=\"https://inet.detik.com/fotoinet/d-4087155/banjir-meme-lucu-jerman-dilibas-korsel?_ga=2.114805636.2000521913.1530092963-1820897464.1527539558\" style=\"font-family: &quot;Source Sans Pro&quot;, sans-serif; font-size: 30px; background-color: rgb(255, 255, 255);\">Banjir Meme Lucu Jerman Dilibas Korsel</a></p>\r\n<p>\r\n	<strong>detikInet | Kamis, 28 Jun 2018 06:40 WIB</strong></p>\r\n<p>\r\n	Meme lucu masih terus banjir di media sosial pasca kekalahan Jerman dari Korea Selatan yang membuat sang juara bertahan tersingkir dari Piala Dunia 2018.</p>\r\n<p>\r\n	<img alt=\"\" src=\"https://akcdn.detik.net.id/community/media/visual/2018/06/28/d6cc0c8d-8c4b-492e-bd31-449d61d74aeb.png?w=780&amp;q=90\" style=\"width: 200px; height: 199px;\"></p>\r\n<p>\r\n	<img alt=\"\" src=\"https://inet.detik.com/fotoinet/d-4087155/banjir-meme-lucu-jerman-dilibas-korsel?_ga=2.102166018.2000521913.1530092963-1820897464.1527539558\"></p>\r\n', '<div>\r\n	悲惨な洪水ミームドイツ南部を打ち負かす</div>\r\n<div>\r\n	detikInet | 木曜日、2018年6月28日06:40 WIB</div>\r\n<div>\r\n	 </div>\r\n<div>\r\n	2018年のワールドカップでディフェンディングチャンピオンを排除した韓国からのドイツの敗北の後、ソーシャルメディアで悲しいままになったミーム。</div>\r\n<div>\r\n	<img alt=\"\" src=\"https://akcdn.detik.net.id/community/media/visual/2018/06/28/d6cc0c8d-8c4b-492e-bd31-449d61d74aeb.png?w=780&q=90\" style=\"width: 200px; height: 199px;\" /></div>\r\n<p>\r\n	 </p>\r\n', '2018-07-03 11:36:25', 1),
(2, 'Test berita menyambut KORBAN', 'テストニュース歓迎Qurban', '<p>\r\n	Berita utama ini cukup penting bagi anda dan perlu anda ketahui</p>\r\n<p>\r\n	<img alt=\"\" src=\"http://localhost/fw-master/assets/uploads/134a9-Ciri-ciri-Hewan-Kurban-Yang-Baik.png\" style=\"width: 300px; height: 183px;\" /></p>\r\n', '<p>\r\n	この見出しはあなたにとって非常に重要で、あなたは知る必要があります この見出しはあなたにとって非常に重要で、あなたは知る必要があります</p>\r\n<p>\r\n	<img alt=\"\" src=\"http://localhost/fw-master/assets/uploads/134a9-Ciri-ciri-Hewan-Kurban-Yang-Baik.png\" style=\"width: 300px; height: 183px;\" /></p>\r\n<p>\r\n	 </p>\r\n', '2018-07-03 22:22:05', 1),
(3, 'Test berita utama dari jepang', '日本のニュースの見出しをテストする', '<p>\r\n	Kami melaporkan berita di Indonesia sesegera mungkin dalam bahasa Jepang, dan juga memposting berita yang terkait dengan Jepang dan India. Dari informasi tentang supermarket makanan Jepang hingga berita komunitas dan panduan acara dari masyarakat Jepang, kami memberikan informasi yang berguna untuk kehidupan pembaca Jepang</p>\r\n', '<p>\r\n	できるだけ早くインドネシアでのニュースを日本語で、また日本とインドに関するニュースを掲載しています。 日本の食品スーパーマーケットから、日本社会のコミュニティニュースやイベントガイドまで、日本の読者の生活に役立つ情報を提供しています</p>\r\n', '2018-06-27 22:46:47', 1);

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE `group` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`id`, `name`, `description`) VALUES
(1, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id`, `nama`) VALUES
(1, 'FISIP'),
(2, 'MATEMATIKA'),
(3, 'KOMPUTER');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `log_action`
--

CREATE TABLE `log_action` (
  `id` int(11) NOT NULL,
  `datetime_event` timestamp NOT NULL DEFAULT current_timestamp(),
  `user_id` int(11) DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  `detail` mediumtext DEFAULT NULL,
  `ip` varchar(55) NOT NULL,
  `page` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_action`
--

INSERT INTO `log_action` (`id`, `datetime_event`, `user_id`, `action`, `detail`, `ip`, `page`) VALUES
(2669, '2018-07-17 03:33:23', 1, 'Jenis Kelamin', 'Tambah Data:Jenis Kelamin', '::1', 80),
(2670, '2018-07-17 03:34:27', 1, 'Jenis Kelamin', 'Hapus Data:Jenis Kelamin', '::1', 81),
(2671, '2018-07-17 03:41:52', 1, 'Jenis Kelamin', 'Tambah Data:Jenis Kelamin', '::1', 82),
(2672, '2018-07-17 03:42:20', 1, 'Jenis Kelamin', 'Hapus Data:Jenis Kelamin', '::1', 83),
(2673, '2018-07-17 04:15:41', 1, 'Jenis Kelamin', 'Tambah Data:Jenis Kelamin', '::1', 84),
(2674, '2018-07-17 04:15:47', 1, 'Jenis Kelamin', 'Hapus Data:Jenis Kelamin', '::1', 85),
(2675, '2018-07-17 15:51:54', 1, 'Jenis Kelamin', 'Merubah Data:Jenis Kelamin', '::1', 86),
(2676, '2018-07-17 15:52:32', 1, 'Jenis Kelamin', 'Merubah Data:Jenis Kelamin', '::1', 87),
(2677, '2018-07-17 15:57:32', 1, 'Jenis Kelamin', 'Merubah Data:Jenis Kelamin', '::1', 88),
(2678, '2018-07-17 15:58:14', 1, 'Jenis Kelamin', 'Merubah Data:Jenis Kelamin', '::1', 89),
(2679, '2018-07-17 15:59:35', 1, 'Jenis Kelamin', 'Merubah Data:Jenis Kelamin', '::1', 90),
(2680, '2018-07-17 16:03:17', 1, 'Jenis Kelamin', 'Merubah Data:Jenis Kelamin', '::1', 91),
(2681, '2018-07-17 16:06:10', 1, 'Jenis Kelamin', 'Merubah Data:Jenis Kelamin', '::1', 92),
(2682, '2018-07-17 16:07:26', 1, 'Jenis Kelamin', 'Merubah Data:Jenis Kelamin', '::1', 93),
(2683, '2018-07-17 16:09:24', 1, 'Jenis Kelamin', 'Merubah Data:Jenis Kelamin', '::1', 94),
(2684, '2018-07-17 16:14:06', 1, 'Jenis Kelamin', 'Merubah Data:Jenis Kelamin', '::1', 95),
(2685, '2018-07-17 16:15:45', 1, 'Jenis Kelamin', 'Merubah Data:Jenis Kelamin', '::1', 96),
(2686, '2018-07-17 16:18:01', 1, 'Jenis Kelamin', 'Merubah Data:Jenis Kelamin', '::1', 97),
(2687, '2018-07-17 16:21:38', 1, 'Jenis Kelamin', 'Merubah Data:Jenis Kelamin', '::1', 98),
(2688, '2018-07-17 16:25:52', 1, 'Jenis Kelamin', 'Merubah Data:Jenis Kelamin', '::1', 99),
(2689, '2018-07-17 16:25:57', 1, 'Jenis Kelamin', 'Merubah Data:Jenis Kelamin', '::1', 100),
(2690, '2018-07-17 16:28:39', 1, 'Jenis Kelamin', 'Merubah Data:Jenis Kelamin', '::1', 101),
(2691, '2018-07-18 01:58:34', 1, 'Login System', '<p>Log In Berhasil</p> webmaster', '::1', 0),
(2692, '2018-07-18 07:28:59', 1, 'Login System', '<p>Log In Berhasil</p> webmaster', '::1', 0),
(2693, '2018-07-22 04:34:09', 1, 'Login System', '<p>Log In Berhasil</p> webmaster', '::1', 0),
(2694, '2018-07-22 09:31:15', 1, 'Login System', '<p>Log In Berhasil</p> webmaster', '::1', 0),
(2695, '2018-07-22 09:49:44', 1, 'Login Out', 'Log Out System ', '::1', 0),
(2696, '2018-08-24 22:57:38', 1, 'Login System', '<p>Log In Berhasil</p> webmaster', '::1', 0),
(2697, '2018-08-25 02:13:39', 1, 'Media', 'Tambah Data:Media', '::1', 102),
(2698, '2018-08-25 02:16:08', 1, 'Media', 'Hapus Data:Media', '::1', 103),
(2699, '2018-08-25 04:46:27', 1, 'Login System', '<p>Log In Berhasil</p> webmaster', '::1', 0),
(2700, '2018-11-08 15:47:46', 1, 'Login System', '<p>Log In Berhasil</p> webmaster', '127.0.0.1', 0),
(2701, '2018-12-13 14:20:24', 1, 'Login System', '<p>Log In Berhasil</p> webmaster', '::1', 0),
(2702, '2019-06-13 02:03:40', 1, 'Login System', '<p>Log In Berhasil</p> webmaster', '::1', 0),
(2703, '2019-07-31 04:35:50', 1, 'Login System', '<p>Log In Berhasil</p> webmaster', '::1', 0),
(2704, '2019-09-02 23:35:37', 0, 'Login Out', 'Log Out System ', '::1', 0),
(2705, '2019-09-02 23:35:51', 1, 'Login System', '<p>Log In Berhasil</p> webmaster', '::1', 0),
(2706, '2019-09-02 23:45:42', 1, 'Login System', '<p>Log In Berhasil</p> member@member.com', '::1', 0),
(2707, '2019-09-02 23:58:43', 1, 'Login System', '<p>Log In Berhasil</p> member@member.com', '::1', 0),
(2708, '2019-09-09 21:26:29', 1, 'Login System', '<p>Log In Berhasil</p> webmaster', '::1', 0),
(2709, '2019-09-18 04:57:38', 1, 'Login System', '<p>Log In Berhasil</p> webmaster', '::1', 0),
(2710, '2019-09-18 04:58:14', 1, 'Login System', '<p>Log In Berhasil</p> webmaster', '::1', 0),
(2711, '2019-09-18 04:58:32', 1, 'Login System', '<p>Log In Berhasil</p> webmaster', '::1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `log_change`
--

CREATE TABLE `log_change` (
  `id` int(11) NOT NULL,
  `datetime_event` timestamp NOT NULL DEFAULT current_timestamp(),
  `user_id` int(11) DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  `old` mediumtext DEFAULT NULL,
  `new` mediumtext DEFAULT NULL,
  `ip` varchar(55) NOT NULL,
  `page` int(11) DEFAULT 0 COMMENT '0:frontend;1:backend'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `log_change`
--

INSERT INTO `log_change` (`id`, `datetime_event`, `user_id`, `action`, `old`, `new`, `ip`, `page`) VALUES
(80, '2018-07-17 03:33:23', 1, 'Tambah Jenis Kelamin', 'N/A', 'Array\n(\n    [jenis_kelamin] => RETESTE\n)\n', '::1', 0),
(81, '2018-07-17 03:34:27', 1, 'Edit Jenis Kelamin', '', '', '::1', 0),
(82, '2018-07-17 03:41:52', 1, 'Tambah Jenis Kelamin', 'N/A', 'Array\n(\n    [jenis_kelamin] => TESTER\n)\n', '::1', 0),
(83, '2018-07-17 03:42:20', 1, 'Hapus Jenis Kelamin', '', '', '::1', 0),
(84, '2018-07-17 04:15:41', 1, 'Tambah Jenis Kelamin', 'N/A', 'Array\n(\n    [jenis_kelamin] => ALAYERS\n)\n', '::1', 0),
(85, '2018-07-17 04:15:47', 1, 'Hapus Jenis Kelamin', 'Array\n(\n    [0] => Array\n        (\n            [id] => 10\n            [jenis_kelamin] => ALAYERS\n        )\n\n)\n', '', '::1', 0),
(86, '2018-07-17 15:51:54', 1, 'Edit Jenis Kelamin', 'Array\n(\n    [0] => Array\n        (\n            [id] => 2\n            [jenis_kelamin] => PEREMPUAN\n        )\n\n)\n', 'Array\n(\n    [jenis_kelamin] => PEREMPUANX\n)\n', '::1', 0),
(87, '2018-07-17 15:52:32', 1, 'Edit Jenis Kelamin', 'Array\n(\n    [0] => Array\n        (\n            [id] => 2\n            [jenis_kelamin] => PEREMPUANX\n        )\n\n)\n', 'Array\n(\n    [jenis_kelamin] => PEREMPUAN\n)\n', '::1', 0),
(88, '2018-07-17 15:57:32', 1, 'Edit Jenis Kelamin', 'Array\n(\n    [0] => Array\n        (\n            [id] => 2\n            [jenis_kelamin] => PEREMPUAN\n        )\n\n)\n', 'Array\n(\n    [jenis_kelamin] => PEREMPUANX\n)\n', '::1', 0),
(89, '2018-07-17 15:58:14', 1, 'Edit Jenis Kelamin', 'Array\n(\n    [0] => Array\n        (\n            [id] => 2\n            [jenis_kelamin] => PEREMPUANX\n        )\n\n)\n', 'Array\n(\n    [jenis_kelamin] => PEREMPUAN\n)\n', '::1', 0),
(90, '2018-07-17 15:59:35', 1, 'Edit Jenis Kelamin', 'Array\n(\n    [0] => Array\n        (\n            [id] => 2\n            [jenis_kelamin] => PEREMPUAN\n        )\n\n)\n', 'Array\n(\n    [jenis_kelamin] => PEREMPUAN\n)\n', '::1', 0),
(91, '2018-07-17 16:03:17', 1, 'Edit Jenis Kelamin', 'Array\n(\n    [0] => Array\n        (\n            [id] => 1\n            [jenis_kelamin] => LAKI-LAKI\n        )\n\n)\n', 'Array\n(\n    [jenis_kelamin] => LAKI-LAKIX\n)\n', '::1', 0),
(92, '2018-07-17 16:06:10', 1, 'Edit Jenis Kelamin', 'Array\n(\n    [0] => Array\n        (\n            [id] => 1\n            [jenis_kelamin] => LAKI-LAKIX\n        )\n\n)\n', 'Array\n(\n    [jenis_kelamin] => LAKI-LAKI\n)\n', '::1', 0),
(93, '2018-07-17 16:07:26', 1, 'Edit Jenis Kelamin', 'Array\n(\n    [0] => stdClass Object\n        (\n            [id] => 1\n            [jenis_kelamin] => LAKI-LAKI\n        )\n\n)\n', 'Array\n(\n    [jenis_kelamin] => LAKI-LAKIX\n)\n', '::1', 0),
(94, '2018-07-17 16:09:24', 1, 'Edit Jenis Kelamin', 'Array\n(\n    [0] => Array\n        (\n            [id] => 1\n            [jenis_kelamin] => LAKI-LAKIX\n        )\n\n)\n', 'Array\n(\n    [jenis_kelamin] => LAKI-LAKI\n)\n', '::1', 0),
(95, '2018-07-17 16:14:06', 1, 'Edit Jenis Kelamin', 'Array\n(\n    [0] => Array\n        (\n            [id] => 1\n            [jenis_kelamin] => LAKI-LAKI\n        )\n\n)\n', 'Array\n(\n    [jenis_kelamin] => LAKI-LAKIX\n)\n', '::1', 0),
(96, '2018-07-17 16:15:45', 1, 'Edit Jenis Kelamin', 'Array\n(\n    [0] => Array\n        (\n            [id] => 1\n            [jenis_kelamin] => LAKI-LAKIX\n        )\n\n)\n', 'Array\n(\n    [jenis_kelamin] => LAKI-LAKIX\n)\n', '::1', 0),
(97, '2018-07-17 16:18:01', 1, 'Edit Jenis Kelamin', 'Array\n(\n    [0] => Array\n        (\n            [id] => 1\n            [jenis_kelamin] => LAKI-LAKIX\n        )\n\n)\n', 'Array\n(\n    [jenis_kelamin] => LAKI-LAKI\n)\n', '::1', 0),
(98, '2018-07-17 16:21:38', 1, 'Edit Jenis Kelamin', 'Array\n(\n    [0] => Array\n        (\n            [id] => 1\n            [jenis_kelamin] => LAKI-LAKI\n        )\n\n)\n', 'Array\n(\n    [jenis_kelamin] => LAKI-LAKIX\n)\n', '::1', 0),
(99, '2018-07-17 16:25:52', 1, 'Edit Jenis Kelamin', 'Array\n(\n)\n', 'Array\n(\n)\n', '::1', 0),
(100, '2018-07-17 16:25:57', 1, 'Edit Jenis Kelamin', 'Array\n(\n)\n', 'Array\n(\n)\n', '::1', 0),
(101, '2018-07-17 16:28:39', 1, 'Edit Jenis Kelamin', 'Array\n(\n    [0] => Array\n        (\n            [id] => 1\n            [jenis_kelamin] => LAKI-LAKI\n        )\n\n)\n', 'Array\n(\n    [jenis_kelamin] => LAKI-LAKIX\n)\n', '::1', 0),
(102, '2018-08-25 02:13:39', 1, 'Tambah Media', 'N/A', 'Array\n(\n    [s572d4e42] => \n    [url] => e7b99-hae.png\n    [title] => \n    [order] => \n)\n', '::1', 0),
(103, '2018-08-25 02:16:07', 1, 'Hapus Media', 'Array\n(\n    [0] => Array\n        (\n            [id] => 3\n            [url] => e7b99-hae.png\n            [title] => \n            [order] => \n        )\n\n)\n', '', '::1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL DEFAULT 0,
  `nim` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telp` int(11) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `foto` varchar(50) NOT NULL DEFAULT '0',
  `keterangan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `id_jurusan`, `nim`, `nama`, `email`, `telp`, `tgl_lahir`, `foto`, `keterangan`) VALUES
(2, 1, NULL, 'Ratih', NULL, NULL, NULL, '', NULL),
(3, 2, NULL, 'Laurin', NULL, NULL, NULL, '', NULL),
(4, 1, NULL, 'Ferry', NULL, NULL, NULL, '', NULL),
(5, 0, NULL, 'Kamil', NULL, NULL, NULL, '', NULL),
(6, 0, NULL, 'Rini Aprilia', NULL, NULL, NULL, '', NULL),
(7, 0, NULL, 'Fadila', NULL, NULL, NULL, '', NULL),
(8, 0, NULL, 'Leni', NULL, NULL, NULL, '', NULL),
(9, 0, NULL, 'Azis', NULL, NULL, NULL, '', NULL),
(10, 0, NULL, 'Izam', NULL, NULL, NULL, '', NULL),
(11, 0, NULL, 'Indra', NULL, NULL, NULL, '', NULL),
(12, 0, NULL, 'Nindy', NULL, NULL, NULL, '', NULL),
(13, 0, NULL, 'Wahyudi', 'wahyudisumadi@gmail.com', 2147483647, '1976-04-18', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_anggota`
--

CREATE TABLE `master_anggota` (
  `id` int(11) NOT NULL,
  `nama_anggota` varchar(255) NOT NULL,
  `pekerjaan` varchar(255) NOT NULL,
  `perusahaan` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `alamat` varchar(1024) NOT NULL,
  `no_telfon` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_anggota`
--

INSERT INTO `master_anggota` (`id`, `nama_anggota`, `pekerjaan`, `perusahaan`, `jenis_kelamin`, `alamat`, `no_telfon`) VALUES
(1, 'arfin', 'arfn', 'dmks', 'perempuan', 'JALAN SENEN', 89328382);

-- --------------------------------------------------------

--
-- Table structure for table `master_buku`
--

CREATE TABLE `master_buku` (
  `id` int(11) NOT NULL,
  `judul_buku` varchar(1024) NOT NULL,
  `pengarang_buku` varchar(255) NOT NULL,
  `tahun_terbit` int(11) NOT NULL,
  `jumlah_halaman` int(11) NOT NULL,
  `jumlah_buku` int(11) NOT NULL,
  `kategori_buku` varchar(1024) NOT NULL,
  `keterangan_buku` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_buku`
--

INSERT INTO `master_buku` (`id`, `judul_buku`, `pengarang_buku`, `tahun_terbit`, `jumlah_halaman`, `jumlah_buku`, `kategori_buku`, `keterangan_buku`) VALUES
(1, 'dsa', 'dsa', 1991, 10, 2147483647, 'fantasi', '21321321'),
(2, 'JUDUL 2', 'JUDUL 2', 1991, 10, 2147483647, 'JUDUL 2', 'JUDUL 2');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) DEFAULT NULL,
  `file_url` varchar(255) DEFAULT NULL,
  `width` float DEFAULT NULL,
  `height` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `identifier`, `file_url`, `width`, `height`) VALUES
(1, 'logo', 'c3d3d-framework_master_orange.png', 220, NULL),
(2, 'small_logo', '51ec5-framework_master_light.png', 80, 50);

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `id_peminjam` int(11) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `nama_anggota` varchar(255) NOT NULL,
  `judul_buku` varchar(1024) NOT NULL,
  `durasi` int(11) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `tgl_kembali` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `id_peminjam`, `id_buku`, `nama_anggota`, `judul_buku`, `durasi`, `tgl_pinjam`, `tgl_kembali`) VALUES
(1, 1, 2, 'arfin', 'JUDUL 2', 14, '2020-11-14', '2020-11-28'),
(2, 1, 1, 'arfin', 'dsa', 14, '2020-11-14', '2020-11-28');

-- --------------------------------------------------------

--
-- Table structure for table `pengembalian`
--

CREATE TABLE `pengembalian` (
  `id` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `site_access`
--

CREATE TABLE `site_access` (
  `id` int(11) NOT NULL,
  `datetime_event` timestamp NOT NULL DEFAULT current_timestamp(),
  `page_id` int(11) DEFAULT NULL,
  `ip` varchar(55) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT 999
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_access`
--

INSERT INTO `site_access` (`id`, `datetime_event`, `page_id`, `ip`, `user_id`) VALUES
(17995, '2019-09-03 00:01:38', 0, '::1', 1),
(17994, '2019-09-03 00:01:05', 0, '::1', 1),
(17993, '2019-09-03 00:00:16', 0, '::1', 1),
(17992, '2019-09-02 23:59:56', 0, '::1', 1),
(17991, '2019-09-02 23:58:44', 0, '::1', 1),
(17990, '2019-09-02 23:57:25', 0, '::1', 1),
(17989, '2019-09-02 23:56:15', 0, '::1', 1),
(17988, '2019-09-02 23:45:43', 0, '::1', 1),
(17987, '2019-09-02 23:24:24', 0, '::1', 0),
(17986, '2019-09-02 23:19:10', 0, '::1', 0),
(17985, '2019-09-02 23:18:23', 0, '::1', 0),
(17984, '2019-09-02 22:58:51', 0, '::1', 0),
(17983, '2019-09-01 23:00:14', 0, '::1', 0),
(17982, '2019-08-27 02:27:07', 0, '::1', 0),
(17981, '2019-07-31 04:35:38', 0, '::1', 0),
(17980, '2019-06-20 23:09:09', 0, '::1', 1),
(17979, '2019-06-13 17:52:09', 0, '::1', 0),
(17978, '2019-06-13 17:25:01', 0, '::1', 0),
(17977, '2019-06-13 02:03:24', 0, '::1', 1),
(17976, '2019-05-20 16:11:23', 0, '::1', 0),
(17975, '2019-05-12 00:35:04', 0, '::1', 0),
(17974, '2018-11-08 15:47:35', 0, '127.0.0.1', 0),
(17973, '2018-09-09 00:51:14', 0, '::1', 0),
(17972, '2018-08-25 16:11:49', 0, '::1', 0),
(17971, '2018-08-25 16:03:10', 0, '::1', 0),
(17970, '2018-08-25 16:01:35', 0, '::1', 0),
(17969, '2018-08-25 16:01:25', 0, '::1', 0),
(17968, '2018-08-25 16:00:45', 0, '::1', 0),
(17967, '2018-08-25 15:57:23', 0, '::1', 0),
(17966, '2018-08-25 15:48:11', 0, '::1', 0),
(17965, '2018-08-25 13:31:07', 0, '::1', 0),
(17964, '2018-08-25 10:56:23', 0, '::1', 0),
(17963, '2018-08-25 10:55:37', 0, '::1', 0),
(17962, '2018-08-25 10:53:50', 0, '::1', 0),
(17961, '2018-08-25 10:25:03', 0, '::1', 0),
(17960, '2018-08-25 10:21:42', 0, '::1', 0),
(17959, '2018-08-25 10:20:47', 0, '::1', 0),
(17958, '2018-08-25 10:20:20', 0, '::1', 0),
(17957, '2018-08-25 10:13:14', 0, '::1', 0),
(17956, '2018-08-25 10:10:00', 0, '::1', 0),
(17955, '2018-08-25 10:04:54', 0, '::1', 0),
(17954, '2018-08-25 10:04:24', 0, '::1', 0),
(17953, '2018-08-25 09:58:58', 0, '::1', 0),
(17952, '2018-08-25 09:57:14', 0, '::1', 0),
(17951, '2018-08-25 09:56:55', 0, '::1', 0),
(17950, '2018-08-25 09:56:39', 0, '::1', 0),
(17949, '2018-08-25 09:53:04', 0, '::1', 0),
(17948, '2018-08-25 09:52:48', 0, '::1', 0),
(17947, '2018-08-25 09:51:28', 0, '::1', 0),
(17946, '2018-08-25 09:50:29', 0, '::1', 0),
(17945, '2018-08-25 09:50:04', 0, '::1', 0),
(17944, '2018-08-25 09:47:52', 0, '::1', 0),
(17943, '2018-08-25 09:47:25', 0, '::1', 0),
(17942, '2018-08-25 09:46:20', 0, '::1', 0),
(17941, '2018-08-25 09:45:22', 0, '::1', 0),
(17940, '2018-08-25 09:42:59', 0, '::1', 0),
(17939, '2018-08-25 09:41:29', 0, '::1', 0),
(17938, '2018-08-25 09:39:18', 0, '::1', 0),
(17937, '2018-08-25 09:36:31', 0, '::1', 0),
(17936, '2018-08-25 09:35:11', 0, '::1', 0),
(17935, '2018-08-25 09:32:54', 0, '::1', 0),
(17934, '2018-08-25 09:31:33', 0, '::1', 0),
(17933, '2018-08-25 09:23:32', 0, '::1', 0),
(17932, '2018-08-25 09:22:28', 0, '::1', 0),
(17931, '2018-08-25 09:20:18', 0, '::1', 0),
(17930, '2018-08-25 09:14:30', 0, '::1', 0),
(17929, '2018-08-25 09:06:06', 0, '::1', 0),
(17928, '2018-08-25 09:05:44', 0, '::1', 0),
(17927, '2018-08-25 09:05:06', 0, '::1', 0),
(17926, '2018-08-25 09:04:23', 0, '::1', 0),
(17925, '2018-08-25 09:03:51', 0, '::1', 0),
(17924, '2018-08-25 09:02:55', 0, '::1', 0),
(17923, '2018-08-25 09:02:32', 0, '::1', 0),
(17922, '2018-08-25 09:02:09', 0, '::1', 0),
(17921, '2018-08-25 09:01:48', 0, '::1', 0),
(17920, '2018-08-25 09:01:26', 0, '::1', 0),
(17919, '2018-08-25 09:00:47', 0, '::1', 0),
(17918, '2018-08-25 09:00:20', 0, '::1', 0),
(17917, '2018-08-25 08:59:55', 0, '::1', 0),
(17916, '2018-08-25 08:59:38', 0, '::1', 0),
(17915, '2018-08-25 08:59:16', 0, '::1', 0),
(17914, '2018-08-25 08:58:44', 0, '::1', 0),
(17913, '2018-08-25 08:58:15', 0, '::1', 0),
(17912, '2018-08-25 08:57:45', 0, '::1', 0),
(17911, '2018-08-25 08:53:32', 0, '::1', 0),
(17910, '2018-08-25 08:53:03', 0, '::1', 0),
(17909, '2018-08-25 08:50:04', 0, '::1', 0),
(17908, '2018-08-25 08:49:33', 0, '::1', 0),
(17907, '2018-08-25 08:48:58', 0, '::1', 0),
(17906, '2018-08-25 08:48:24', 0, '::1', 0),
(17905, '2018-08-25 08:46:03', 0, '::1', 0),
(17904, '2018-08-25 08:45:46', 0, '::1', 0),
(17903, '2018-08-25 08:22:01', 0, '::1', 0),
(17902, '2018-08-25 08:17:50', 0, '::1', 0),
(17901, '2018-08-25 08:17:44', 0, '::1', 0),
(17900, '2018-08-25 08:16:33', 0, '::1', 0),
(17899, '2018-08-25 08:14:22', 0, '::1', 0),
(17898, '2018-08-25 08:11:22', 0, '::1', 0),
(17897, '2018-08-25 08:10:50', 0, '::1', 0),
(17896, '2018-08-25 08:10:42', 0, '::1', 0),
(17895, '2018-08-25 08:08:59', 0, '::1', 0),
(17894, '2018-08-25 08:08:29', 0, '::1', 0),
(17893, '2018-08-25 08:05:05', 0, '::1', 0),
(17892, '2018-08-25 08:03:57', 0, '::1', 0),
(17891, '2018-08-25 08:02:58', 0, '::1', 0),
(17890, '2018-08-25 07:57:42', 0, '::1', 0),
(17889, '2018-08-25 07:55:52', 0, '::1', 0),
(17888, '2018-08-25 07:54:12', 0, '::1', 0),
(17887, '2018-08-25 07:53:17', 0, '::1', 0),
(17886, '2018-08-25 07:53:09', 0, '::1', 0),
(17885, '2018-08-25 07:52:35', 0, '::1', 0),
(17884, '2018-08-25 07:50:43', 0, '::1', 0),
(17883, '2018-08-25 07:45:32', 0, '::1', 0),
(17882, '2018-08-25 07:37:30', 0, '::1', 0),
(17881, '2018-08-25 07:37:18', 0, '::1', 0),
(17880, '2018-08-25 07:35:34', 0, '::1', 0),
(17879, '2018-08-25 07:32:48', 0, '::1', 0),
(17878, '2018-08-25 07:32:40', 0, '::1', 0),
(17877, '2018-08-25 07:32:19', 0, '::1', 0),
(17876, '2018-08-25 07:29:13', 0, '::1', 0),
(17875, '2018-08-25 07:19:43', 0, '::1', 0),
(17874, '2018-08-25 07:19:38', 0, '::1', 0),
(17873, '2018-08-25 07:19:27', 0, '::1', 0),
(17872, '2018-08-25 06:08:06', 0, '::1', 0),
(17871, '2018-08-25 00:03:08', 0, '::1', 0),
(17870, '2018-08-25 00:03:04', 0, '::1', 0),
(17869, '2018-08-25 00:02:58', 0, '::1', 0),
(17868, '2018-08-25 00:00:29', 0, '::1', 0),
(17867, '2018-08-25 00:00:26', 0, '::1', 0),
(17866, '2018-08-25 00:00:22', 0, '::1', 0),
(17865, '2018-08-25 00:00:15', 0, '::1', 0),
(17864, '2018-08-24 23:58:23', 0, '::1', 0),
(17863, '2018-08-24 23:58:19', 0, '::1', 0),
(17862, '2018-08-24 23:52:20', 0, '::1', 0),
(17861, '2018-08-24 23:31:57', 0, '::1', 0),
(17996, '2019-09-03 00:09:24', 0, '::1', 1),
(17997, '2019-09-03 00:10:36', 0, '::1', 1),
(17998, '2019-09-03 00:11:22', 0, '::1', 1),
(17999, '2019-09-03 00:12:25', 0, '::1', 1),
(18000, '2019-09-08 02:27:41', 0, '::1', 1),
(18001, '2019-09-08 08:07:54', 0, '::1', 0),
(18002, '2019-09-09 21:26:16', 0, '::1', 0),
(18003, '2019-09-09 21:32:01', 0, '::1', 0),
(18004, '2019-09-09 22:29:12', 0, '::1', 0),
(18005, '2019-09-09 22:45:42', 0, '::1', 0),
(18006, '2019-09-18 04:57:28', 0, '::1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `site_map`
--

CREATE TABLE `site_map` (
  `id` int(11) NOT NULL,
  `page` varchar(55) DEFAULT NULL,
  `access` int(11) DEFAULT 0 COMMENT '0:frontend;1:backend'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_map`
--

INSERT INTO `site_map` (`id`, `page`, `access`) VALUES
(1, 'Home', 0),
(2, 'Home', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'member', 'member', NULL, 'member@member.com', NULL, NULL, NULL, NULL, 1451903855, 1567468723, 1, 'Member', 'One', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `absensi_mahasiswa`
--
ALTER TABLE `absensi_mahasiswa`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `admin_groups`
--
ALTER TABLE `admin_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_login_attempts`
--
ALTER TABLE `admin_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_users_groups`
--
ALTER TABLE `admin_users_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `api_access`
--
ALTER TABLE `api_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `api_keys`
--
ALTER TABLE `api_keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `api_limits`
--
ALTER TABLE `api_limits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `api_logs`
--
ALTER TABLE `api_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_action`
--
ALTER TABLE `log_action`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_change`
--
ALTER TABLE `log_change`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_anggota`
--
ALTER TABLE `master_anggota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_buku`
--
ALTER TABLE `master_buku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`),
  ADD KEY `id_peminjam` (`id_peminjam`),
  ADD KEY `id_buku` (`id_buku`);

--
-- Indexes for table `site_access`
--
ALTER TABLE `site_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_map`
--
ALTER TABLE `site_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `absensi_mahasiswa`
--
ALTER TABLE `absensi_mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `admin_groups`
--
ALTER TABLE `admin_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `admin_login_attempts`
--
ALTER TABLE `admin_login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `admin_users_groups`
--
ALTER TABLE `admin_users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `api_access`
--
ALTER TABLE `api_access`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `api_keys`
--
ALTER TABLE `api_keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `api_limits`
--
ALTER TABLE `api_limits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `api_logs`
--
ALTER TABLE `api_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log_action`
--
ALTER TABLE `log_action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2712;

--
-- AUTO_INCREMENT for table `log_change`
--
ALTER TABLE `log_change`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `master_anggota`
--
ALTER TABLE `master_anggota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `master_buku`
--
ALTER TABLE `master_buku`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `site_access`
--
ALTER TABLE `site_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18007;

--
-- AUTO_INCREMENT for table `site_map`
--
ALTER TABLE `site_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD CONSTRAINT `id_buku` FOREIGN KEY (`id_buku`) REFERENCES `master_buku` (`id`),
  ADD CONSTRAINT `id_peminjam` FOREIGN KEY (`id_peminjam`) REFERENCES `master_anggota` (`id`);

--
-- Constraints for table `pengembalian`
--
ALTER TABLE `pengembalian`
  ADD CONSTRAINT `fk_buku` FOREIGN KEY (`id_buku`) REFERENCES `peminjaman` (`id_buku`),
  ADD CONSTRAINT `id_anggota` FOREIGN KEY (`id_anggota`) REFERENCES `peminjaman` (`id_peminjam`),
  ADD CONSTRAINT `id_peminjaman` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id_peminjaman`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
