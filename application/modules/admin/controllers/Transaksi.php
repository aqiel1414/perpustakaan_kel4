<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Transaksi extends Admin_Controller {
    public function peminjaman()
	{
		$crud = $this->generate_crud('peminjaman');
		$this->mPageTitle = 'peminjaman';
		$this->render_crud();
	}

	public function pengembalian()
	{
		$crud = $this->generate_crud('pengembalian');
		$this->mPageTitle = 'pengembalian';
		$this->render_crud();
	}
}